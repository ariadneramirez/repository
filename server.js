var path =require('path');
var sql = require('mssql');

const crypto = require('crypto');

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const url = require('url');

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

var dbConfig = {
	server: '201.148.26.102',
	database: 'PruebasDB',
	user: 'PruebasMayanGPS',
	password: 'Mayan17019pruebas',
	port: 1433
};

var conn = new sql.ConnectionPool(dbConfig);
var req = new sql.Request(conn);

conn.connect(function (err) {

	if (err) {
		console.log(err);
		console.log("Conexiòn no satisfactoria. ");
		return;
	}

	req.query("SELECT UsuarioID FROM Usuarios", function (err, recordset) {

		if (err) {
			console.log(err);
			console.log("Conexiòn no satisfactoria x2. ");
		} else {
			console.log(recordset);
			console.log("Conexiòn satisfactoria. ");
		}

		// conn.close();

	});

});

/*app.set('views',path.join(__dirname,'views'));
app.engine('html',require('ejs').renderFile);
app.set('view engine', 'ejs');*/

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


//app.use(require('./routes/index'));

// login
app.post('/entrar', (req, res) => {
	let data = { usuario : req.body.usuario, pass : req.body.pass };
	let sql = `SELECT * FROM Usuarios WHERE Usuario = '${data.usuario}' AND Contra = '${data.pass}'`;
	let query = conn.query(sql, (err, results) => {
		if (results) {

			idusuario = "";

			for (var i = 0; i < results.recordset.length; i++) {

				idusuario = results.recordset[i].UsuarioID;

			} 

			const key = "";

			if (idusuario) {

				// crear sessid
				const key = crypto.randomBytes(10).toString('hex');

				let sql = `INSERT INTO Sesiones(U_Key, UsuarioID) VALUES('${key}', '${idusuario}')`;
				let query = conn.query(sql, (err, results) => {

					if (results) {
						console.log(results);

					} else {
						console.log(err);

					}

				});

				res.send(key);
				console.log(key);

			} else {

				res.send(key);
				console.log(key);			

			}

		} else {

			console.log(err);

		}

	});

});

// verificar disponibilidad
app.post('/verificar', (req, res) => {
	let data = { usuario : req.body.usuario };
	let sql = `SELECT * FROM Usuarios WHERE Usuario = '${data.usuario}'`;
	let query = conn.query(sql, (err, results) => {
		if (results) {

			var usuarios = results.recordset.length;
			res.send(String(usuarios));

		} 
	});
});

// registrar
app.post('/registrar', (req, res) => {
	let data = { usuario: req.body.usuario, pass: req.body.pass, descr: req.body.descrip }
	let sql = `INSERT INTO Usuarios(Usuario, Contra, Descrip) VALUES ('${data.usuario}', '${data.pass}', '${data.descr}')`;
	let query = conn.query(sql, (err, results) => {

		if (results) {
			console.log("Usuario registrado con èxito. ");
			res.redirect("/home.html");
		} else {
			console.log(err);
		}
	});
});

// crear sessid
app.post('/sessid', (req, res) => {
	let data = { usuario : req.body.usuario };
	let sql = `SELECT * FROM Usuarios WHERE Usuario = '${data.usuario}'`;

	let query = conn.query(sql, (err, results) => {

		if (results) {

			for (var i = 0; i < results.recordset.length; i++) {
				idusuario = results.recordset[i].UsuarioID;
			}

			const id = crypto.randomBytes(10).toString('hex');

			let sql = `INSERT INTO Sesiones(U_Key, UsuarioID) VALUES('${id}', '${idusuario}')`;
			let query = conn.query(sql, (err, results) => {

				if (results) {

					let sql = `SELECT * FROM Sesiones WHERE U_Key = '${id}'`;
					let query = conn.query(sql, (err, results) => {

						if (results) {

							for (var j = 0; j < results.recordset.length; j++) {
								key = results.recordset[j].U_Key;
							}

							res.send(key);

						}
					});

				} else {
					console.log(err);
				}

			});
		}
	});

});

// verificar existencia
app.post('/existencia', (req, res) => {
	let data = { key : req.body.key_u };
	let sql = `SELECT * FROM Sesiones WHERE U_Key = '${data.key}'`;
	let query = conn.query(sql, (err, results) => {
		if (results) {
		}
	});
});

// cargar datos en perfil
app.post('/cargarDatos', (req, res) => {
	let data = { key : req.body.key };
	let sql = `SELECT * FROM Usuarios INNER JOIN Sesiones ON Usuarios.UsuarioID = Sesiones.UsuarioID WHERE Sesiones.U_Key = '${data.key}'`;
	let query = conn.query(sql, (err, results) => {
		if (results) {

			for (var i = 0; i < results.recordset.length; i++) {

				var datos = {
								usuario : results.recordset[i].Usuario,
						 		passwdr : results.recordset[i].Contra,
								descrip : results.recordset[i].Descrip
							};
				
			}

			res.send(datos);

		} else {
			console.log("No se ha iniciado sesiòn. ");
		}
	});
});

// disponibilidad
app.post('/disponibilidad', (req, res) => {
	let data = { key : req.body.key };
	let sql = `SELECT Usuarios.UsuarioID, Usuarios.Usuario FROM Usuarios INNER JOIN Sesiones ON Usuarios.UsuarioID = Sesiones.UsuarioID WHERE U_Key = '${data.key}'`;
	let query = conn.query(sql, (err, results) => {
		if (results) {

			for (var i = 0; i < results.recordset.length; i++) {
				usuario = results.recordset[i].Usuario;
			}

			let sql = `SELECT Usuario FROM Usuarios WHERE Usuario != '${usuario}'`;
			let query = conn.query(sql, (err, results) => {
				if (results) {

					user = req.body.user;
					userd = "";
					for (var i = 0; i < results.recordset.length; i++) {
						if (user == results.recordset[i].Usuario) {
							userd = results.recordset[i].Usuario;
						} 
					}

					datos = { usuario : userd };

					res.send(datos);
				}
			});
		}
	});
});

// actualizar datos de perfil
app.post('/actualizar', (req, res) => {
	let data = { key : req.body.key };
	let sql = `SELECT Usuarios.UsuarioID FROM Usuarios INNER JOIN Sesiones ON Usuarios.UsuarioID = Sesiones.UsuarioID WHERE U_Key = '${data.key}'`;
	let query = conn.query(sql, (err, results) => {
		if (results) {

			for (var i = 0; i < results.recordset.length; i++) {
				idusuario = results.recordset[i].UsuarioID;
			}

			let data = { usuario : req.body.usuario, passwdr : req.body.passwdr, descrip : req.body.descrip };
			let sql = `UPDATE Usuarios SET Usuario = '${data.usuario}', Contra = '${data.passwdr}', Descrip = '${data.descrip}'  WHERE UsuarioID = ${idusuario}`;

			let query = conn.query(sql, (err, results) => {

				if (results) {

					res.send(data);
					console.log(results);

				} else {

					console.log(err);
				
				}

			});

		} else {
			console.log(err);
		}

	});
});

// eliminar cuenta
app.post('/eliminar', (req, res) => {
	let data = { key : req.body.key };
	let sql = `SELECT Usuarios.UsuarioID FROM Usuarios INNER JOIN Sesiones ON Usuarios.UsuarioID = Sesiones.UsuarioID WHERE U_Key = '${data.key}'`;
	let query = conn.query(sql, (err, results) => {
		
		if (results) {

			for (var i = 0; i < results.recordset.length; i++) {
				idu = results.recordset[i].UsuarioID;
			}

			let sql = `DELETE FROM Sesiones WHERE UsuarioID = ${idu}`;
			let query = conn.query(sql, (err, results) => {
				if (results) {
				}
			});

			let sql_2 = `DELETE FROM Usuarios WHERE UsuarioID = ${idu}`;
			let query_2 = conn.query(sql_2, (err, results) => {
				if (results) {
				}
			});

			res.send(String(idu));

		}

	});		

});

// eliminar sessid
app.post('/cerrarsesion', (req, res) => {
	let data = { key : req.body.key };
	let sql = `DELETE FROM Sesiones WHERE U_Key = '${data.key}'`;
	let query = conn.query(sql, (err, results) => {
		if (results) {
			console.log(results);
		}
	});
});


app.set('port',3000);

app.use(express.static('./'));

app.listen(app.get('port'),()=>{
	console.log('server on port', app.get('port'));
});



