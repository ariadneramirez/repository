
// LOGIN

$("#btnEntrar").click(function() {

	if ($("#txtEmail").val() == "") {

		alert("Es necesario llenar el campo de Usuario. ");

	} else if ($("#txtPassword").val() == "") {

		alert("Es necesario llenar el campo de Contraseña. ");

	} else {

		login();

	}

});

function login() {

	$.ajax({

		url: "http://192.168.1.130:3000/entrar",
		data: { usuario : $("#txtEmail").val(),
				pass : $("#txtPassword").val() },
		type: 'POST',

		success: function(data) {

			console.log(data);

			var key_u = data;
			sessionStorage.setItem("Key", key_u);

			if (key_u == null || key_u == "") {

				alert("Usuario y Contraseña ingresados no existen o son incorrectos. Verifique su informaciòn. ");
				$("#txtEmail").val("");
				$("#txtPassword").val("");

			} else {

				alert("Usuario y Contraseña ingresados correctamente. Bienvenido, " + $("#txtEmail").val() + ".");
				location.href = "home.html";

			}
			
		},

		error: function(err) {

			console.log(err);

			alert("Usuario y Contraseña ingresados no existen o son incorrectos. Verifique su informaciòn. ");
			$("#txtEmail").val("");
			$("#txtPassword").val("");
			
		}

	});


}

