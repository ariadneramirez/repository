// cargar datos de sesiòn
cargarDatos();

// botones

$("#btnActualizar").click(function() {

	key = sessionStorage.getItem("Key");

	if (key == null || key == "") {

		alert("No se ha iniciado sesiòn. ");
    	location.href = "index.html";

	} else {

    	location.href = "perfil_edicion.html";

	}

});

$("#btnLogout").click(function() {

	key = sessionStorage.getItem("Key");

	if (key == null || key == "") {

		alert("No se ha iniciado sesiòn. ");
    	location.href = "index.html";

    } else {

		cerrarSesion();
		sessionStorage.clear();
		location.href = "index.html";
		
	}

});

$("#btnEliminar").click(function() {

	key = sessionStorage.getItem("Key");

	if (key == null || key == "") {

		alert("No se han podido eliminar la cuenta ya que no hay sesiòn iniciada actualmente. ");
    	location.href = "index.html";

	} else {

		$("#confirmacion").modal("show");

	}
	
});

$("#btnAceptar").click(function() {

	key = sessionStorage.getItem("Key");

	if (key == null || key == "") {

		alert("No se han podido eliminar la cuenta ya que no hay sesiòn iniciada actualmente. ");
    	location.href = "index.html";

	} else {

		eliminarCuenta();

	}

});

// funciones

function cargarDatos() {

    key = sessionStorage.getItem("Key");

  	if (key != null) {

  		$.ajax({

			url: "http://192.168.1.130:3000/cargarDatos",
			data: { key : key },
			type: 'POST',

			success: function(data) {

				console.log(data);
				$("#txtuser").val(data.usuario);
				$("#txtpassp").val(data.passwdr);
				$("#txtarea_p").val(data.descrip);

			},

			error: function(err) {

				console.log(err);

			}

		});

  	} else {

  		alert("No has iniciado sesiòn. ");
  		location.href = "index.html";

  	}

}

function eliminarCuenta() {

	$.ajax({

		url: "http://192.168.1.130:3000/eliminar",
		data: { key : sessionStorage.getItem("Key") },
		type: 'POST',

		success: function(data) {

			console.log(data);
			sessionStorage.clear();
			alert("El usuario ha sido eliminado con èxito. ");
			location.href = "index.html";

		}, 

		error: function(err) {

			$("#confirmacion").modal("hide");
			$("#negacion").modal("show");

		}

	});

}
 
function cerrarSesion() {

	$.ajax({

		url: "http://192.168.1.130:3000/cerrarsesion",
		data: { key : sessionStorage.getItem("Key") },
		type: 'POST',

		success: function(data) {

			console.log(data);

		},

		error: function(err) {

			console.log(err);

		}

	});

}
