// verificar sessid 
verificarSessid();
// revisar sesiòn

// mapa inicial 
var mymap = L.map('mapid').setView([25.16517337, -102.74414063], 5);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'

  }).addTo(mymap);


//obtener ubicacion
$("#btnUbicar").click(function() {

  key = sessionStorage.getItem("Key");

  if (key == null || key == "") {

    alert("No se han podido obtener la Ubicaciòn actual ya que no hay sesiòn iniciada actualmente. ");
    location.href = "index.html";

  } else {

    getMarker();

  }
  
});

$("#btnLogout").click(function() {

  key = sessionStorage.getItem("Key");

  if (key == null || key == "") {

    alert("No se ha iniciado sesiòn. ");
    location.href = "index.html";

  } else {

    cerrarSesion();
    sessionStorage.clear();
    location.href = "index.html";
    
  }

});

// funciones

function cerrarSesion() {

  $.ajax({

    url: "http://192.168.1.130:3000/cerrarsesion",
    data: { key : sessionStorage.getItem("Key") },
    type: 'POST',

    success: function(data) {

      console.log(data);

    },

    error: function(err) {

      console.log(err);

    }

  });

}

function verificarSessid() {

  key = sessionStorage.getItem("Key");

  if (key == null || key == "") {

    alert("No se ha iniciado sesiòn. ");
    location.href = "index.html";

  } else {

    $.ajax({

      url: "http://192.168.1.130:3000/existencia",
      data: { key_u : key },
      type: 'POST',

      success: function(data) {
        if (data) {

        } else {

          alert("No se ha iniciado sesiòn. ");
          location.href = "index.html";

        }
      }

    });

  }

}

// funciones del mapa
var marker;

function getMarker() {

  navigator.geolocation.getCurrentPosition(function(location) {

    var latlng = new L.LatLng(location.coords.latitude, location.coords.longitude);

    mymap.setView([location.coords.latitude, location.coords.longitude], 16);

    marker = L.marker(latlng).addTo(mymap);

    // agregar fecha al pop-up del mapa
    var fecha = new Date();
    var mes = fecha.getUTCMonth() + 1; 
    var dia = fecha.getUTCDate();
    var anio = fecha.getUTCFullYear();
    var hora = fecha.getHours();
    var minuto = fecha.getMinutes();

    marker.on('click', function(e) {

      var popup = L.popup()
        .setLatLng(latlng)
        .setContent('<p>Ubicaciòn actual: ' + latlng + '<br />Fecha/hora de consulta: ' + dia + '/' + mes + '/' + anio + ' ' + hora + ':' + minuto + '</p>')
        .openOn(mymap);

    });

  });

}

