

// REGISTRO

$("#btnAceptar").click(function() {

	if ($("#txtUsuario").val() == "") {

		alert("Es necesario llenar el campo de Usuario. ");

	} else if ($("#txtPassReg").val() == "") {

		alert("Es necesario llenar el campo de Contraseña. ");

	} else if ($("#txtDescrip").val() == "") {

		alert("Es necesario llenar el campo de Descripciòn. ");
		
	} else {

		// validar disponibilidad del nombre de usuario
		$.ajax({
			url: "http://192.168.1.130:3000/verificar",
			data: { usuario : $("#txtUsuario").val() },
			type: 'POST',

			success: function(data) {

				if (data != "0") {

					console.log(data);
					alert("El Nombre de Usuario ingresado ya se encuentra registrado. Favor de intentar con uno diferente. ");
					$("#txtUsuario").val("");


				} else {

					// registrar al usuario
					$.ajax({

						url: "http://192.168.1.130:3000/registrar",
						data: {
								usuario : $("#txtUsuario").val(),
								pass : $("#txtPassReg").val(),
								descrip : $("#txtDescrip").val()
							  },
						type: 'POST',

						success: function(data) {
							
							if (data) {
								
								// console.log(data);
								crearSessid();

								alert("Usuario registrado con èxito. Bienvenido, " + $("#txtUsuario").val() + ".");
								location.href = "home.html";

							} else {
								alert("No se ha podido registrar al Usuario. Intèntelo de nuevo. ");
							}

						},

						error: function(err) {
							alert("No se ha podido registrar al Usuario. Intèntelo de nuevo. ");
						}

					});

				}
			
			},

			error: function(err) {
				alert("El Usuario ingresado ya se encuentra registrado. Favor de intentar con uno diferente. ");
				$("#txtUsuario").val("");
			}

		});



	}

});


$("#btnCancelar").click(function(){

	$("#txtUsuario").val("");
	$("#txtPassReg").val("");
	$("#txtDescrip").val("");

	location.href = "index.html";

});

function crearSessid() {
	// aqui va el de crear sessid

	$.ajax({

		url: "http://192.168.1.130:3000/sessid",
		data: { usuario : $("#txtUsuario").val() },
		type: 'POST',

		success: function(data) {

			if (data) {
				var key_u = data;
				sessionStorage.setItem("Key", key_u);
			} 

		}, 

		error: function(err) {

			console.log(err);

		}

	});

}