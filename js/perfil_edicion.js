cargarDatos();

// botones
$("#btnCancelar").click(function() {

	key = sessionStorage.getItem("Key");

	if (key == null || key == "") {

		alert("No se ha iniciado sesiòn. ");
    	location.href = "index.html";

	} else {

		location.href = "perfil.html";

	}

});

$("#btnLogout").click(function() {

	key = sessionStorage.getItem("Key");

	if (key == null || key == "") {

		alert("No se ha iniciado sesiòn. ");
    	location.href = "index.html";

    } else {

		cerrarSesion();
		sessionStorage.clear();
		location.href = "index.html";
		
	}

});

$("#btnGuardar").click(function() {

	key = sessionStorage.getItem("Key");

	if (key == null || key == "") {

		alert("No se ha podido actualizar la informaciòn ya que no hay sesiòn iniciada actualmente. ");
    	location.href = "index.html";

	} else {

    	$("#confirmacion").modal("show");

	}

});

$("#btnAceptar").click(function() {

	key = sessionStorage.getItem("Key");

	if (key == null || key == "") {

		alert("No se ha podido actualizar la informaciòn ya que no hay sesiòn iniciada actualmente. ");
    	location.href = "index.html";

	} else {

		verificarUsuario();
		$("#confirmacion").modal("hide");

	}

});

// funciones
function actualizar() {

	key = sessionStorage.getItem("Key");

	if (key == null) {

		alert("No se han podido actualizar los datos ya que no hay sesiòn iniciada actualmente. ");
		location.href = "index.html";
	
	} else {

		$.ajax({

			url: "http://192.168.1.130:3000/actualizar",
			data: { 
					key : sessionStorage.getItem("Key"),
					usuario : $("#txtUser").val(), 
					passwdr : $("#txtContra").val(),
					descrip : $("#txtDescrip").val()
				  },
			type: 'POST',

			success: function(data) {

				console.log(data);
				
				$("#txtUser").val(data.usuario);
				$("#txtContra").val(data.passwdr);
				$("#txtDescrip").val(data.descrip);

				alert("El Usuario ha sido actualizado con èxito. ");

			}, 

			error: function(err) {

				alert("El Usuario no fue actualizado correctamente. ");

			}

		});

	}

}

function cargarDatos() {

    key = sessionStorage.getItem("Key");

  	if (key != null) {

		$.ajax({

			url: "http://192.168.1.130:3000/cargarDatos",
			data: { key : key },
			type: 'POST',

			success: function(data) {

				console.log(data);

				$("#txtUser").val(data.usuario);
				$("#txtContra").val(data.passwdr);
				$("#txtDescrip").val(data.descrip);

			},

			error: function(err) {

				console.log(err);

			}

		});

	} else {

		alert("No has iniciado sesiòn. ");
		location.href = "index.html";

	}

}

function cerrarSesion() {

	$.ajax({

		url: "http://192.168.1.130:3000/cerrarsesion",
		data: { key : sessionStorage.getItem("Key") },
		type: 'POST',

		success: function(data) {

			console.log(data);

		},

		error: function(err) {

			console.log(err);

		}

	});

}

function verificarUsuario() {

	$.ajax({

		url: "http://192.168.1.130:3000/disponibilidad",
		data: { key : sessionStorage.getItem("Key"),
				user : $("#txtUser").val() },
		type: 'POST',

		success: function(data) {
			
			if (data.usuario == "") {
				actualizar();
			} else {
				alert("El usuario capturado no se encuentra disponible. Favor de intentar con otro diferente. ");
			}

		},

		error: function(err) {

			console.log(err);

		}

	});

}

